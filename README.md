# Features of the multi-chip Carboard v1.4

* FMC mezzanine — FMC HPC Connector
* Chip Board Connector — [Samtec SEAF](https://www.samtec.com/products/seaf}{https://www.samtec.com/products/seaf) 320 Pins
* 8 × general purpose power supplies with monitoring capabilities
 * Maximum current: 3 A
 * Voltage range 0.8 — 3.6 V
* 32 × adjustable voltage output (0 — 4 V)
* 8 × current output (0 — 1 mA)
* 8 × voltage input (0 — 4 V)
* [FEASTMP](http://project-dcdc.web.cern.ch/project-dcdc/Default.html) support 
 * DCDC converters for applications in High Energy Physics
* 8× full-duplex SERDES links
* [ADC](http://www.analog.com/en/products/analog-to-digital-converters/ad9249.html) (16 channels, 65 MSPS/14-bit)
* 4 × injection pulser
* HV input
* I2C bus
* TLU RJ45 input (clock and trigger/shutter)
* general CMOS signals (10 × outputs, 14 × inputs) with adjustable voltage levels (0.8 - 3.6V)
* 17 × LVDS pairs (CML converters only on the specific chipboards)
* output jitter attenuator/clock multiplier ([SI5345](http://www.silabs.com/products/timing/clocks/high-performance-jitter-attenuators/device.si5345b))
 * Inputs: quartz, TLU, FMC, EXT (UMC)
 * Outputs: 3 × FMC (including GBT), 2 × SEAF, 1 × ADC
 * 0-delay mode
 
![picture alt](https://gitlab.cern.ch/Caribou/Caribou-HW-CaR/raw/master/doc/CaR.JPG)
